<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Multiplicar del 9</title>
    <style>
        tr:nth-child(even) {
            background-color: #f2f2f2; 
        }
        tr:nth-child(odd) {
            background-color: #ffffff; 
        }
    </style>
</head>
<body>
    <h1>Tabla de Multiplicar del 9</h1>
    <table border="1">
        <tr>
            <th>Número</th>
            <th>Resultado</th>
        </tr>
        <?php
        for ($i = 1; $i <= 10; $i++) {
            $resultado = $i * 9;
            $filaClass = ($i % 2 == 0) ? "par" : "impar";
            echo '<tr class="' . $filaClass . '">';
            echo '<td>' . $i . '</td>';
            echo '<td>' . $resultado . '</td>';
            echo '</tr>';
        }
        ?>
    </table>
</body>
</html>
