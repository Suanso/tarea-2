<!DOCTYPE html>
<html>
<head>
    <title>Concatenación de String</title>
</head>
<body>
    <?php
    $cadena1 = "String 1, ";
    $cadena2 = "String 2";
    $resultado = $cadena1 . $cadena2;

    echo "<p>Los Strings concatenados son: $resultado</p>";
    ?>
</body>
</html>
