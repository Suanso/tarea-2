<!DOCTYPE html>
<html>
<head>
    <title>Tipo de Datos en PHP</title>
</head>
<body>
    <?php
    $variable = 24.5;

    echo "El tipo de datos de la variable es: " . gettype($variable) . "<br>";

    $variable = "HOLA";

    echo "El tipo de datos de la variable es: " . gettype($variable) . "<br>";

    settype($variable, "integer");

    var_dump($variable);
    ?>
</body>
</html>
