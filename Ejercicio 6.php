<?php
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);

$nota_acumulada = $parcial1 + $parcial2 + $final1;

switch (true) {
    case ($nota_acumulada >= 90 && $nota_acumulada <= 100):
        $nota_final = "Nota 5";
        break;
    case ($nota_acumulada >= 80 && $nota_acumulada < 90):
        $nota_final = "Nota 4";
        break;
    case ($nota_acumulada >= 70 && $nota_acumulada < 80):
        $nota_final = "Nota 3";
        break;
    case ($nota_acumulada >= 60 && $nota_acumulada < 70):
        $nota_final = "Nota 2";
        break;
    default:
        $nota_final = "Nota 1";
        break;
}

echo "La nota final del alumno es: " . $nota_final;
?>
